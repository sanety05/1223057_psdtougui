﻿using PhotoshopFile;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;


public class PSDEditorWindow : EditorWindow
{
    private Font font;
    private Texture2D image;
    private Vector2 scrollPos;
    private PsdFile psd;
    private int atlassize = 4096;
    private float pixelsToUnitSize = 100.0f;
    private string fileName;
    private List<string> LayerList = new List<string>();
    private bool ShowAtlas;
    private GameObject CanvasObj;
    private string PackingTag;
    private float referenceResX = 800f;
    private float referenceResY = 600f;

    #region Const


    const string Button = "button";
    const string Highlight = "highlight";
    const string Disable = "disable";
    const string Touched = "touched";

    #endregion

    #region MenuItems

    [MenuItem("Window/uGUI/PSD Converter")]
    public static void ShowWindow()
    {
        var wnd = GetWindow<PSDEditorWindow>();
	
        wnd.title = "PSD To uGUI";
        wnd.minSize = new Vector2(400, 300);
        wnd.Show();
    }


	
	
    [MenuItem("Assets/Convert to uGUI", true, 20000)]
    private static bool saveLayersEnabled()
    {
        for (var i = 0; i < Selection.objects.Length; i++)
        {
			
			
            var obj = Selection.objects[i];
            var filePath = AssetDatabase.GetAssetPath(obj);
            if (filePath.EndsWith(".psd", System.StringComparison.CurrentCultureIgnoreCase))
            {
                return true;
            }
        }
		
        return false;
    }

    [MenuItem("Assets/Convert to uGUI", false, 20000)]
    private  static void saveLayers()
    {
		
		
        var obj = Selection.objects[0];

        var window = EditorWindow.GetWindow<PSDEditorWindow>(true, "PSD to uGUI ");
        window.minSize = new Vector2(400, 300);
        window.image = (Texture2D)obj;
        window.LoadInformation(window.image);
        window.Show();
			
		
		
		
		
    }

    #endregion



    public void OnGUI()
    {
        
        EditorGUI.BeginChangeCheck();

        image = (Texture2D)EditorGUILayout.ObjectField("PSD File", image, typeof(Texture2D), true);

        bool changed = EditorGUI.EndChangeCheck();

        if (image != null)
        {
            if (changed)
            {
                string path = AssetDatabase.GetAssetPath(image);

                if (path.ToUpper().EndsWith(".PSD", System.StringComparison.CurrentCultureIgnoreCase))
                {
                    LoadInformation(image);
                }
                else
                {
                    psd = null;
                }
              
            }
//            if (font == null)
//            {
//                EditorGUILayout.HelpBox("Choose the font of text layers.", MessageType.Error);
//            }
            font = (Font)EditorGUILayout.ObjectField("Font", font, typeof(Font), true);
            if (psd != null)
            {

                //scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
                {
                    #region toggles
//                    foreach (Layer layer in psd.Layers)
//                    {
//                        var sectionInfo = (LayerSectionInfo)layer.AdditionalInfo
//												.SingleOrDefault(x => x is LayerSectionInfo);
//                        if (sectionInfo == null)
//                        {
//											
//                            layer.Visible = EditorGUILayout.ToggleLeft(layer.Name, layer.Visible);
//                        }
//
//                    }
                    #endregion

                    #region select button
//                    EditorGUILayout.BeginHorizontal();
//                    {
//                        if (GUILayout.Button("Select All", GUILayout.Width(200)))
//                        {
//                            foreach (Layer layer in psd.Layers)
//                            {
//                                var sectionInfo = (LayerSectionInfo)layer.AdditionalInfo
//                                .SingleOrDefault(x => x is LayerSectionInfo);
//                                if (sectionInfo == null)
//                                {
//
//                                    layer.Visible = true;
//                                }
//
//                            }
//
//                        }
//                        if (GUILayout.Button("Select None", GUILayout.Width(200)))
//                        {
//                            foreach (Layer layer in psd.Layers)
//                            {
//                                var sectionInfo = (LayerSectionInfo)layer.AdditionalInfo
//                                .SingleOrDefault(x => x is LayerSectionInfo);
//                                if (sectionInfo == null)
//                                {
//
//                                    layer.Visible = false;
//                                }
//
//                            }
//
//                        }
//                    }
//                    EditorGUILayout.EndHorizontal();
                    #endregion

                    #region packing tag
                    EditorGUILayout.BeginHorizontal();
                    {
                        GUILayout.Label("Packing Tag");
                        PackingTag = EditorGUILayout.TextArea(PackingTag);
                    }
                    EditorGUILayout.EndHorizontal(); 
                    #endregion

                    #region resolution
                    EditorGUILayout.BeginVertical();
                    {
                        GUILayout.Label("Resolution");
                        EditorGUILayout.BeginHorizontal();
                        {
                            GUILayout.Label("X");
                            referenceResX = EditorGUILayout.FloatField(referenceResX);
                            GUILayout.Label("Y");
                            referenceResY = EditorGUILayout.FloatField(referenceResY);
                            GUILayout.FlexibleSpace();
                        }
                        EditorGUILayout.EndHorizontal();
                    }
                    EditorGUILayout.EndVertical();
                    #endregion
                }
                //EditorGUILayout.EndScrollView();


//                if (GUILayout.Button("Create atlas && GUI "))
//                {
//                    ShowAtlas = !ShowAtlas;
//                }
//                if (ShowAtlas)
//                {
//                    atlassize = EditorGUILayout.IntField("Max. atlas size", atlassize);
//					
//                    if (!((atlassize != 0) && ((atlassize & (atlassize - 1)) == 0)))
//                    {
//                        EditorGUILayout.HelpBox("Atlas size should be a power of 2", MessageType.Warning);
//                    }
//					
//                    //pixelsToUnitSize = EditorGUILayout.FloatField("Pixels To Unit Size", pixelsToUnitSize);
//					
//                    if (pixelsToUnitSize <= 0)
//                    {
//                        EditorGUILayout.HelpBox("Pixels To Unit Size should be greater than 0.", MessageType.Warning);
//                    }	
//
//                }

                if (GUILayout.Button("Convert"))
                {
                    CreateAtlas();                                                  
                }

//                if (GUILayout.Button("Create Folder With Sprites && GUI"))
//                {
//                    ExportLayers();
//                }
		
            }
            else
            {
                EditorGUILayout.HelpBox("This texture is not a PSD file.", MessageType.Error);
            }
        }
    }

    private Texture2D CreateTexture(Layer layer)
    {
        if ((int)layer.Rect.width == 0 || (int)layer.Rect.height == 0)
        {
            return null;
        }

        Texture2D tex = new Texture2D((int)layer.Rect.width, (int)layer.Rect.height, TextureFormat.RGBA32, true);
        Color32[] pixels = new Color32[tex.width * tex.height];
        Channel red = (from l in layer.Channels
                       where l.ID == 0
                       select l).First();
        Channel green = (from l in layer.Channels
                         where l.ID == 1
                         select l).First();
        Channel blue = (from l in layer.Channels
                        where l.ID == 2
                        select l).First();
        Channel alpha = layer.AlphaChannel;
        for (int i = 0; i < pixels.Length; i++)
        {
            byte r = red.ImageData[i];
            byte g = green.ImageData[i];
            byte b = blue.ImageData[i];
            byte a = 255;
            if (alpha != null)
            {
                a = alpha.ImageData[i];
            }
            int mod = i % tex.width;
            int n = ((tex.width - mod - 1) + i) - mod;
            pixels[pixels.Length - n - 1] = new Color32(r, g, b, a);
        }

        tex.SetPixels32(pixels);
        tex.Apply();
        return tex;
    }

    private void CreateAtlas()
    {
        List<Texture2D> textures = new List<Texture2D>();
        List<SpriteRenderer> spriteRenderers = new List<SpriteRenderer>();
        LayerList = new List<string>();
        int zOrder = 0;
        GameObject root = new GameObject(fileName);
        foreach (var layer in psd.Layers)
        {
            if (layer.Visible && layer.Rect.width > 0 && layer.Rect.height > 0)
            {
                if (LayerList.IndexOf(layer.Name.Split('|').Last()) == -1)
                {
                    LayerList.Add(layer.Name.Split('|').Last());
                    Texture2D tex = CreateTexture(layer);
                    textures.Add(tex);
                    GameObject go = new GameObject(layer.Name);
                    SpriteRenderer sr = go.AddComponent<SpriteRenderer>();
                    go.transform.position = new Vector3((layer.Rect.width / 2 + layer.Rect.x) / pixelsToUnitSize,
                        (-layer.Rect.height / 2 - layer.Rect.y) / pixelsToUnitSize, 0);
                    spriteRenderers.Add(sr);
                    sr.sortingOrder = zOrder++;
                    go.transform.parent = root.transform;
                }
            }
        }
        Rect[] rects;
        Texture2D atlas = new Texture2D(atlassize, atlassize);
        Texture2D[] textureArray = textures.ToArray();
        rects = atlas.PackTextures(textureArray, 2, atlassize);
        List<SpriteMetaData> Sprites = new List<SpriteMetaData>();
        for (int i = 0; i < rects.Length; i++)
        {
            SpriteMetaData smd = new SpriteMetaData();
            smd.name = spriteRenderers[i].name.Split('|').Last();
            smd.rect = new Rect(rects[i].xMin * atlas.width,
                rects[i].yMin * atlas.height, 
                rects[i].width * atlas.width, 
                rects[i].height * atlas.height);
            smd.pivot = new Vector2(0.5f, 0.5f);
            smd.alignment = (int)SpriteAlignment.Center;
            Sprites.Add(smd);
        }

        string assetPath = AssetDatabase.GetAssetPath(image);
        string path = Path.Combine(Path.GetDirectoryName(assetPath),
                    Path.GetFileNameWithoutExtension(assetPath) + "_atlas" + ".png");
			
        byte[] buf = atlas.EncodeToPNG();
        File.WriteAllBytes(path, buf);
        AssetDatabase.Refresh();

        atlas = (Texture2D)AssetDatabase.LoadAssetAtPath(path, typeof(Texture2D));
        var textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;
        textureImporter.maxTextureSize = atlassize;
        textureImporter.spritesheet = Sprites.ToArray();
        textureImporter.textureType = TextureImporterType.Sprite; 
        textureImporter.spriteImportMode = SpriteImportMode.Multiple;
        textureImporter.spritePivot = new Vector2(0.5f, 0.5f);
        textureImporter.spritePixelsPerUnit = pixelsToUnitSize;
        textureImporter.textureFormat = TextureImporterFormat.AutomaticTruecolor;
        AssetDatabase.ImportAsset(path, ImportAssetOptions.Default);

        foreach (Texture2D tex in textureArray)
        {
            DestroyImmediate(tex);
        }

        AssetDatabase.Refresh();
        DestroyImmediate(root);
        Sprite[] atlases = AssetDatabase.LoadAllAssetsAtPath(path).Select(x => x as Sprite).Where(x => x != null).ToArray();
        CreateGUI(atlases);

    }

    private void CreateGUI(Sprite[] atlas)
    {
        for (int i = 0; i < psd.Layers.Count; i++)
        {
            if (((LayerSectionInfo)psd.Layers[i].AdditionalInfo
								.SingleOrDefault(x => x is LayerSectionInfo) == null) && (psd.Layers[i].Visible))
            {

                var RealLayerName = psd.Layers[i].Name.Split('|').Last();
							
                var textlayer = (LayerTextInfo)psd.Layers[i].AdditionalInfo.SingleOrDefault(x => x is LayerTextInfo);


                if (RealLayerName.StartsWith(Button) && RealLayerName.EndsWith(Touched))
                {
                    var temp = GameObject.Find(RealLayerName.Split('_')[0] + "_" + RealLayerName.Split('_')[1]);
                    var State = temp.GetComponent<Selectable>().spriteState;
                    State.pressedSprite = atlas[i];
                    temp.GetComponent<Selectable>().spriteState = State;
                }
                else if (RealLayerName.StartsWith(Button) && RealLayerName.EndsWith(Highlight))
                {
                    var temp = GameObject.Find(RealLayerName.Split('_')[0] + "_" + RealLayerName.Split('_')[1]);
                    var State = temp.GetComponent<Selectable>().spriteState;
                    State.highlightedSprite = atlas[i];
                    temp.GetComponent<Selectable>().spriteState = State;
                }
                else if (RealLayerName.StartsWith(Button) && RealLayerName.EndsWith(Disable))
                {
                    var temp = GameObject.Find(RealLayerName.Split('_')[0] + "_" + RealLayerName.Split('_')[1]);
                    var State = temp.GetComponent<Selectable>().spriteState;
                    State.disabledSprite = atlas[i];
                    temp.GetComponent<Selectable>().spriteState = State;
                }
                else
                {

                    var instant = CreatePanel(psd.Layers[i].Name.Split('|'));
                    instant.name = psd.Layers[i].Name.Split('|').Last();
                    instant.SetActive(false);
				
                    if (textlayer != null)
                    {
						
                        instant.AddComponent<Text>();
                        instant.GetComponent<Text>().text = textlayer.text;
                        instant.GetComponent<Text>().color = ColorPicker(psd.Layers[i]);
                        instant.GetComponent<Text>().font = font;
                        instant.GetComponent<RectTransform>().sizeDelta = new Vector2(atlas[Array.FindIndex(atlas, x => x.name == RealLayerName)].rect.width, atlas[Array.FindIndex(atlas, x => x.name == RealLayerName)].rect.height);
                        instant.GetComponent<Text>().resizeTextForBestFit = true;
                        instant.GetComponent<Text>().raycastTarget = false;
                        
                    }
                    else
                    {
                        instant.AddComponent <Image>();
                        instant.GetComponent <Image>().sprite = atlas[Array.FindIndex(atlas, x => x.name == RealLayerName)];
                        instant.GetComponent<Image>().SetNativeSize();
                        instant.GetComponent<Image>().raycastTarget = false;
                    }
                        
                    instant.SetActive(true);
                    instant.GetComponent<RectTransform>().anchorMax = Vector2.zero;
                    instant.GetComponent<RectTransform>().anchorMin = Vector2.zero;

                    instant.GetComponent<RectTransform>().pivot = Vector2.zero;

                    float yPos = psd.Layers[i].Rect.y;

                    if (Mathf.Abs(yPos) > 0)
                    {
                        yPos = referenceResY - yPos;
                        instant.GetComponent<RectTransform>().pivot = new Vector2(0, 1);
                    }

                    instant.GetComponent<RectTransform>().anchoredPosition = new Vector2(psd.Layers[i].Rect.x,yPos);
                    var temp = instant.transform.parent;
                    instant.transform.GetComponent<RectTransform>().SetParent(temp);
                    			
                    Debug.LogFormat("Name:{0},Rect:{1}", psd.Layers[i].Name, psd.Layers[i].Rect);

                    if (RealLayerName.StartsWith(Button))
                    {
                        instant.AddComponent <Button>();
                        instant.GetComponent<Image>().raycastTarget = true;
                    }
					   
                }
            }		
        }
    }

    GameObject CreatePanel(string[] path)
    {   
        List<string> pathtemp = new List<string>();
        pathtemp.Add("Canvas");
        pathtemp.AddRange(path);
        CanvasObj = GameObject.Find("Canvas");
        List<GameObject> PathObj = new List<GameObject>();
        if (CanvasObj == null)
        {
            CanvasObj = new GameObject();
            CanvasObj.name = "Canvas";
            CanvasObj.AddComponent<Canvas>();
            CanvasObj.AddComponent<CanvasScaler>();
            CanvasObj.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            CanvasObj.GetComponent<CanvasScaler>().uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
            CanvasObj.GetComponent<CanvasScaler>().referenceResolution = new Vector2(referenceResX, referenceResY);
            CanvasObj.GetComponent<CanvasScaler>().screenMatchMode = CanvasScaler.ScreenMatchMode.Expand;
            CanvasObj.AddComponent<GraphicRaycaster>();
            Debug.Log("Created new Canvas");
        }

        PathObj.Add(CanvasObj);
					
        for (int i = 1; i < pathtemp.Count - 1; i++)
        {
				
            if (PathObj[i - 1].transform.FindChild(pathtemp[i]) == null)
            {
                var temp = new GameObject();
                temp.SetActive(false);
                temp.AddComponent <RectTransform>().position = Vector3.zero;
                temp.GetComponent<RectTransform>().sizeDelta = Vector2.zero;
                temp.GetComponent<RectTransform>().anchorMax = Vector2.zero;
                temp.GetComponent<RectTransform>().anchorMin = Vector2.zero;
                temp.transform.GetComponent<RectTransform>().SetParent(PathObj[i - 1].transform);
                temp.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
                temp.name = pathtemp[i];
               
                PathObj.Add(temp);
                temp.SetActive(true);
            }
            else
            {
                PathObj.Add(PathObj[i - 1].transform.FindChild(pathtemp[i]).gameObject);
            }
        }
        var temp1 = new GameObject();
        temp1.SetActive(false);
        temp1.AddComponent <RectTransform>().position = new Vector3(0, 0, 0);
        temp1.name = pathtemp[pathtemp.Count - 1];
        temp1.transform.GetComponent<RectTransform>().SetParent(PathObj[pathtemp.Count - 2].transform);
        PathObj.Add(temp1);
        temp1.SetActive(true);

        return  PathObj.Last();

    }

    public static void ApplyLayerSections(List<Layer> layers)
    {
        var stack = new Stack<string>();


        foreach (var layer in Enumerable.Reverse(layers))
        {

            var sectionInfo = (LayerSectionInfo)layer.AdditionalInfo
								.SingleOrDefault(x => x is LayerSectionInfo);
            if (sectionInfo == null)
            {
                var Reverstack = stack.ToArray();
                Array.Reverse(Reverstack); 
                layer.Name = String.Join("|", Reverstack) + "|" + layer.Name;
            }
            else
            {
                switch (sectionInfo.SectionType)
                { 
                    case LayerSectionType.OpenFolder:
									
										
                        stack.Push(layer.Name);
                        break;
                    case LayerSectionType.Layer:
                        stack.Push(layer.Name);
                        break;
                    case LayerSectionType.ClosedFolder:
										
                        stack.Push(layer.Name);
										
                        break;
                    case LayerSectionType.SectionDivider:
										
										
                        stack.Pop();
                        break;
                }


            }
        }
					
    }


    private void ExportLayers()
    {
        LayerList = new List<string>();
        List<Sprite> atlas = new List<Sprite>();

        string path = AssetDatabase.GetAssetPath(image).Split('.')[0];
	
        Directory.CreateDirectory(path);
        foreach (Layer layer in psd.Layers)
        {
            if (layer.Visible && layer.Rect.width > 0 && layer.Rect.height > 0)
            {
                if (LayerList.IndexOf(layer.Name.Split('|').Last()) == -1)
                {
                    LayerList.Add(layer.Name.Split('|').Last());
                    Texture2D tex = CreateTexture(layer);
                    if (tex == null)
                    {
                        continue;
                    }
                    atlas.Add(SaveAsset(tex, layer.Name.Split('|').Last()));
                    DestroyImmediate(tex);
                }
            }
        }
        CreateGUI(atlas.ToArray());
    }

    private Sprite SaveAsset(Texture2D tex, string syffux)
    {
        string path = AssetDatabase.GetAssetPath(image).Split('.')[0] + "/" + syffux + ".png";
			 
        byte[] buf = tex.EncodeToPNG();
        File.WriteAllBytes(path, buf);
        AssetDatabase.Refresh();
        // Load the texture so we can change the type
        AssetDatabase.LoadAssetAtPath(path, typeof(Texture2D));
        TextureImporter textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;
				
        textureImporter.textureType = TextureImporterType.Sprite; 
        textureImporter.spriteImportMode = SpriteImportMode.Single;
        textureImporter.maxTextureSize = atlassize;
        if (!System.String.IsNullOrEmpty(PackingTag))
        {
            textureImporter.spritePackingTag = PackingTag;
        }

        textureImporter.textureFormat = TextureImporterFormat.AutomaticTruecolor;
        AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
        return (Sprite)AssetDatabase.LoadAssetAtPath(path, typeof(Sprite));
    }

    public void LoadInformation(Texture2D Img)
    {
        string path = AssetDatabase.GetAssetPath(Img);

        psd = new PsdFile(path, Encoding.Default);
        fileName = Path.GetFileNameWithoutExtension(path);
        ApplyLayerSections(psd.Layers);

    }

    public   Color32  ColorPicker(Layer layer)
    {
        Color32[] pixels = new Color32[(int)layer.Rect.width * (int)layer.Rect.height];
        Channel red = (from l in layer.Channels
                       where l.ID == 0
                       select l).First();
        Channel green = (from l in layer.Channels
                         where l.ID == 1
                         select l).First();
        Channel blue = (from l in layer.Channels
                        where l.ID == 2
                        select l).First();
        Channel alpha = layer.AlphaChannel;
        for (int i = 0; i < pixels.Length; i++)
        {
            byte r = red.ImageData[i];
            byte g = green.ImageData[i];
            byte b = blue.ImageData[i];
            byte a = 255;
            if (alpha != null)
            {
                a = alpha.ImageData[i];
            }
            int mod = i % (int)layer.Rect.width;
            int n = (((int)layer.Rect.width - mod - 1) + i) - mod;
            pixels[pixels.Length - n - 1] = new Color32(r, g, b, a);
        }
        int r1 = 0;
        int g1 = 0;
        int b1 = 0;
        byte a1 = 255;
        pixels.ToList().ForEach(delegate(Color32 name)
            {
                r1 += name.r;
                g1 += name.g;
                b1 += name.b;
            }
        );
        return new Color32((byte)(r1 / pixels.Count()), (byte)(g1 / pixels.Count()), (byte)(b1 / pixels.Count()), a1);
	
    }

}


